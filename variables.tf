# Project
variable "name" {
  type        = string
  description = "The name of the new project. Equals path if not provided."
  default     = null
}

variable "path" {
  type        = string
  description = "Repository name for new project."
}

variable "description" {
  type        = string
  description = "Short project description."
  default     = null
}

variable "group_id" {
  type        = number
  description = "Group ID of the new project."
}

variable "ee" {
  type        = bool
  description = "Enable GitLab Enterprise features."
  default     = true
}

variable "topics" {
  type        = set(string)
  description = "The list of topics for a project."
  default     = []
}

variable "default_branch" {
  type        = string
  description = "The default branch name, unmanaged if not set."
  default     = null
}

variable "forked_from_project_id" {
  type        = number
  description = "The ID of the project to fork."
  default     = null
}

variable "initialize_with_readme" {
  type        = bool
  description = "Whether to create a Git repository with just a README.md file."
  default     = null
}

variable "use_existing_project" {
  type        = bool
  description = "Enable this to use an existing project without modifying its current settings."
  default     = false
}

variable "template" {
  type = object({
    name                = optional(string)
    project_id          = optional(number)
    use_custom_template = optional(bool, false)
  })
  description = "Built-in or custom project template settings."
  default     = {}

  validation {
    condition     = !(var.template.name != null && var.template.project_id != null)
    error_message = "template.name and template.project_id cannot be set together."
  }

  validation {
    condition     = var.template.project_id == null || (var.template.project_id != null && var.template.use_custom_template)
    error_message = "use_custom_template must be set to true when template.project_id is set."
  }
}

variable "visibility_level" {
  type        = string
  description = "The project’s visibility. Can be `private`, `internal`, or `public`."
  default     = "public"

  validation {
    condition     = contains(["private", "internal", "public"], var.visibility_level)
    error_message = "The visibility_level value must be \"private\", \"internal\", or \"public\"."
  }
}

variable "request_access_enabled" {
  type        = bool
  description = "Allow users to request member access."
  default     = false
}

# Branch protections
variable "branch_protections" {
  type = map(object({
    code_owner_approval_required = optional(bool)
    merge_access_level           = optional(string, "maintainer")
    push_access_level            = optional(string, "no one")
    unprotect_access_level       = optional(string)
    allow_force_push             = optional(bool)
    allowed_to_merge = optional(object({
      user_ids  = optional(list(number), [])
      group_ids = optional(list(number), [])
    }), {})
    allowed_to_push = optional(object({
      user_ids  = optional(list(number), [])
      group_ids = optional(list(number), [])
    }), {})
    allowed_to_unprotect = optional(object({
      user_ids  = optional(list(number), [])
      group_ids = optional(list(number), [])
    }), {})
  }))
  description = <<-EOF
    Protect repository branches.

    Example:
    {
      my_branch = {
        merge_access_level = "maintainer"
        push_access_level  = "no one"
        allowed_to_push = {
          user_ids = [42, 123]
        }
      }
    }

    Valid access level values: "no one", "developer", or "maintainer"
    EOF
  default     = {}
}

variable "protected_environments" {
  type = map(object({
    approval_rules = optional(list(object({
      access_level           = optional(string)
      group_id               = optional(number)
      group_inheritance_type = optional(number)
      required_approvals     = optional(number)
      user_id                = optional(number)
      })
    ))
    deploy_access_levels = optional(object({
      access_level = optional(string)
      group_ids    = optional(list(number), [])
      user_ids     = optional(list(number), [])
    }))
  }))
  description = <<-EOF
    Protected environments rules, also adds protected variables (Vault roles, etc.).

    Example:
    {
      "my_environment" = {
        approval_rules = {
          required_approvals = 1
        }
        deploy_access_levels = {
          access_level = "maintainer"
          group_ids    = [4]
          user_ids     = [2, 3]
        }
      }
    }
    EOF
  default     = {}
  validation {
    condition = alltrue(
      [for env, rules in var.protected_environments :
      rules.deploy_access_levels.access_level == null ? true : contains(["developer", "maintainer"], rules.deploy_access_levels.access_level)]
    )
    error_message = "The protected_environments.*.deploy_access_levels.access_level value must be \"developer\", or \"maintainer\"."
  }
}

# Tag protections
variable "tag_protections" {
  type = map(object({
    create_access_level = string
    allowed_to_create = optional(object({
      access_level = optional(string)
      user_ids     = optional(list(number), [])
      group_ids    = optional(list(number), [])
    }), {})
  }))
  description = <<-EOF
    Protect repository tags.

    Example:
    {
      "my_tag" = {
        create_access_level = "developer"
      }
    }
    Valid values:
    "no one":     gitlab.NoPermissions,
    "guest":      gitlab.GuestPermissions,
    "reporter":   gitlab.ReporterPermissions,
    "developer":  gitlab.DeveloperPermissions,
    "maintainer": gitlab.MaintainerPermissions,
    "owner":      gitlab.OwnerPermission,
    EOF
  default = {
    "*" = {
      create_access_level = "maintainer"
    }
  }
}

# Issues settings
variable "issues" {
  type = object({
    access_level = optional(string, "enabled")
    template     = optional(string)
  })
  description = "Issues settings for the project."
  default     = {}

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.issues.access_level)
    error_message = "The issues.access_level value must be \"disabled\", \"private\", or \"enabled\"."
  }
}

# MR settings
variable "merge_requests" {
  type = object({
    access_level                                     = optional(string, "enabled")
    approvals_before_merge                           = optional(number, 0)
    author_approval                                  = optional(bool, false)
    default_target_self                              = optional(bool, true)
    disable_committers_approval                      = optional(bool, true)
    disable_overriding_approvers_per_merge_request   = optional(bool, true)
    merge_method                                     = optional(string, "merge")
    merge_pipelines_enabled                          = optional(bool, false)
    merge_trains_enabled                             = optional(bool, false)
    only_allow_merge_if_all_discussions_are_resolved = optional(bool, true)
    allow_merge_on_skipped_pipeline                  = optional(bool, false)
    only_allow_merge_if_pipeline_succeeds            = optional(bool, true)
    remove_source_branch_after_merge                 = optional(bool, true)
    require_password_to_approve                      = optional(bool, false)
    reset_approvals_on_push                          = optional(bool, true)
    selective_code_owner_removals                    = optional(bool, false)
    squash_option                                    = optional(string, "default_off")
    squash_commit_template                           = optional(string)
    commit_template                                  = optional(string)
    template                                         = optional(string)
  })
  description = "Merge Requests settings for the project."
  default     = {}

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.merge_requests.access_level)
    error_message = "The merge_requests.access_level value must be \"disabled\", \"private\", or \"enabled\"."
  }
}

# MR approval rules
variable "approval_rules" {
  type = map(object({
    approvals_required = number
    user_ids           = optional(list(number))
    group_ids          = optional(list(number))
  }))
  description = <<-EOF
    Project-level approval rules.

    Example:
    {
      "my_rule" = {
        approvals_required = 2
        user_ids           = [2, 3]
        group_ids          = [4]
      }
    }
    EOF
  default     = {}
}

variable "pipelines" {
  type = object({
    access_level                  = optional(string, "enabled")
    auto_cancel_pending_pipelines = optional(string)
    ci_config_path                = optional(string)
    ci_default_git_depth          = optional(number)
    ci_forward_deployment_enabled = optional(bool)
    ci_separated_caches           = optional(bool)
    build_timeout                 = optional(number)
    keep_latest_artifact          = optional(bool)
    public_jobs                   = optional(bool)
  })
  description = "Pipelines settings for the project."
  default     = {}

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.pipelines.access_level)
    error_message = "The pipelines.access_level value must be \"disabled\", \"private\", or \"enabled\"."
  }
}

# Project features
variable "container_registry_access_level" {
  type        = string
  description = "Visibility of container registry for the project. Can be `disabled`, `private`, or `enabled`."
  default     = "enabled"

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.container_registry_access_level)
    error_message = "The container_registry_access_level value must be \"disabled\", \"private\", or \"enabled\"."
  }
}

variable "pages_access_level" {
  type        = string
  description = "Visibility of GitLab Pages for the project. Can be `disabled`, `enabled`, `private`, or `public`."
  default     = null

  validation {
    condition     = contains(["disabled", "enabled", "private", "public"], coalesce(var.pages_access_level, "disabled"))
    error_message = "The pages_access_level value must be \"disabled\", \"enabled\", \"private\", or `public`."
  }
}

variable "snippets_access_level" {
  type        = string
  description = "Visibility of the snippets for the project. Can be `disabled`, `private`, or `enabled`."
  default     = "disabled"

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.snippets_access_level)
    error_message = "The snippets_access_level value must be \"disabled\", \"private\", or \"enabled\"."
  }
}

variable "wiki_access_level" {
  type        = string
  description = "Visibility of the wiki for the project. Can be `disabled`, `private`, or `enabled`."
  default     = "disabled"

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.wiki_access_level)
    error_message = "The wiki_access_level value must be \"disabled\", \"private\", or \"enabled\"."
  }
}

variable "lfs_enabled" {
  type        = bool
  description = "Enable/disable Large File Storage (LFS) for the project."
  default     = true
}

variable "packages_enabled" {
  type        = bool
  description = "Enable packages repository for the project."
  default     = true
}

variable "shared_runners_enabled" {
  type        = bool
  description = "Enable shared runners for the project."
  default     = true
}

variable "archived" {
  type        = bool
  description = "Archive the project."
  default     = false
}

# Members
variable "members" {
  type = map(object({
    access_level = string
  }))
  description = <<-EOF
    Project user memberships.

    Example:
    {
      <user_id> = {
        access_level = "developer"
      }
    }
    Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner"
    EOF
  default     = {}
}

# Groups
variable "share_groups" {
  type = map(object({
    group_id     = number
    group_access = string
  }))
  description = <<-EOF
    Share the project with other groups.
    Example:
    {
      <name> = {
        group_id     = 12345
        group_access = "developer"
      }
    }
    Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner"
    EOF
  default     = {}
}

# Vault
variable "vault" {
  type = object({
    enabled                    = optional(bool, false)
    auth_path                  = optional(string, "")
    auth_role_bound_audiences  = optional(list(string), ["https://vault.gitlab.net"])
    auth_role_protected        = optional(string)
    auth_role_protected_suffix = optional(string, "-rw")
    auth_role_readonly         = optional(string)
    # Environment roles, ro == unprotected branch, rw == protected branch
    environment_ro_roles = optional(map(object({
      readonly_secret_paths  = optional(list(string), [])
      readwrite_secret_paths = optional(list(string), [])
      protected              = optional(bool, true)
    })), {})
    environment_rw_roles = optional(map(object({
      readonly_secret_paths  = optional(list(string), [])
      readwrite_secret_paths = optional(list(string), [])
      protected              = optional(bool, true)
    })), {})
    # Non protected branches/environments will get a readonly role
    non_protected_readonly = optional(bool, true)
    # Extra CI secret paths the project should have readonly access to from protected branches/environments only, relative to K/V backend
    protected_secret_paths = optional(list(string), [])
    # Extra CI secret paths the project should have readonly access to, relative to K/V backend
    readonly_secret_paths = optional(list(string), [])
    # Extra CI secret paths the project should have read/write access to, relative to K/V backend
    readwrite_secret_paths = optional(list(string), [])
    # Extra policies for the protected role
    extra_protected_policies = optional(list(string), [])
    # Extra policies for the readonly role
    extra_readonly_policies = optional(list(string), [])
    # Default TTL
    token_ttl = optional(number, 3600) # 1 hour
    # Max TTL
    token_max_ttl = optional(number, 10800) # 3 hours
  })
  description = "Vault settings for the project."
  default     = {}

  validation {
    condition     = var.vault.enabled ? var.vault.auth_path != null && var.vault.auth_path != "" : true
    error_message = "The vault.auth_path value must be set and non-empty when vault.enabled is set to true."
  }
}

variable "common_ci_tasks" {
  type = object({
    enabled                            = optional(bool, false)
    goreleaser                         = optional(bool, true)
    mirroring                          = optional(bool, false)
    renovate_bot                       = optional(bool, true)
    renovate_bot_github_token_path     = optional(string, "shared/renovate/github")
    renovate_bot_schedule              = optional(string)
    semantic_release                   = optional(bool, true)
    danger                             = optional(bool, false)
    republishes_truncated_version_tags = optional(bool, false)
    autolabels                         = optional(bool, true)
  })
  description = "Integration settings for [`common-ci-tasks`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks)."
  default     = {}
}

# Mirrors
variable "pull_mirror" {
  type = object({
    enabled                      = optional(bool, false)
    url                          = optional(string)
    trigger_builds               = optional(bool)
    overwrites_diverged_branches = optional(bool)
  })
  description = "Pull mirror settings for this project."
  default     = {}
}

variable "push_mirrors" {
  type = list(object({
    enabled                 = optional(bool, false)
    url                     = optional(string, "")
    only_protected_branches = optional(bool)
    keep_divergent_refs     = optional(bool)
  }))
  description = "Push mirrors settings for this project."
  default     = []
}

# Job tokens
variable "job_token_scopes" {
  type = object({
    authoritative      = optional(bool, false)
    target_project_ids = optional(set(number), [])
  })
  description = "Job token scopes."
  default     = {}
}

variable "registry_read_token" {
  type        = bool
  description = "Generate a registry read token and put it into Vault."
  default     = false
}
