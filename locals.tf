locals {
  # Project
  project_id                  = var.use_existing_project ? data.gitlab_project.project[0].id : gitlab_project.project[0].id
  project_path_with_namespace = var.use_existing_project ? data.gitlab_project.project[0].path_with_namespace : gitlab_project.project[0].path_with_namespace
  project_full_path_safe      = replace(local.project_path_with_namespace, "/", "_")

  # Common CI Tasks
  common_ci_tasks = {
    goreleaser                         = var.common_ci_tasks.enabled && var.common_ci_tasks.goreleaser && var.pipelines.access_level != "disabled"
    mirroring                          = var.common_ci_tasks.enabled && var.common_ci_tasks.mirroring
    renovate_bot                       = var.common_ci_tasks.enabled && var.common_ci_tasks.renovate_bot && var.merge_requests.access_level != "disabled" && var.pipelines.access_level != "disabled"
    semantic_release                   = var.common_ci_tasks.enabled && var.common_ci_tasks.semantic_release && var.pipelines.access_level != "disabled"
    danger                             = var.common_ci_tasks.enabled && var.common_ci_tasks.danger && var.pipelines.access_level != "disabled"
    republishes_truncated_version_tags = var.common_ci_tasks.enabled && var.common_ci_tasks.semantic_release && var.common_ci_tasks.republishes_truncated_version_tags
    autolabels                         = var.common_ci_tasks.enabled && var.common_ci_tasks.autolabels && var.merge_requests.access_level != "disabled" && var.pipelines.access_level != "disabled"
  }

  # Vault
  vault_enabled = var.vault.enabled && !var.archived

  vault_ci_path               = "ci"
  vault_ci_placeholder_file   = ".placeholder"
  vault_ci_transit_mount_path = "transit/ci"

  vault_ci_jwt_claim_mappings = {
    environment = "environment"
  }

  # JWT backend
  vault_jwt_auth_backend_accessor = local.vault_enabled ? data.vault_auth_backend.jwt[0].accessor : ""
  # Role names
  vault_readonly_role      = local.project_full_path_safe
  vault_protected_role     = "${local.vault_readonly_role}${var.vault.auth_role_protected_suffix}"
  vault_readonly_env_role  = "${local.vault_readonly_role}-env-%s-ro"
  vault_readwrite_env_role = "${local.vault_readonly_role}-env-%s-rw"
  # Policy names
  vault_default_readonly_policy  = "${var.vault.auth_path}-project-${local.project_full_path_safe}"
  vault_default_readwrite_policy = "${local.vault_default_readonly_policy}-rw"
  vault_env_readonly_policy      = "${var.vault.auth_path}-project-${local.project_full_path_safe}-env-%s-ro"
  vault_env_readwrite_policy     = "${var.vault.auth_path}-project-${local.project_full_path_safe}-env-%s-rw"
  # Secrets path
  vault_secrets_path = "${var.vault.auth_path}/${local.project_path_with_namespace}"
  # Extra paths
  vault_kv_v2_expand_regex = "/^(\\w+)\\//"
  vault_readonly_secret_paths = setunion(
    var.vault.readonly_secret_paths,
    module.prat_common_ci_tasks_mirroring[*].vault_secret_path,
    module.prat_common_ci_tasks_danger[*].vault_secret_path,
    module.prat_common_ci_tasks_autolabels[*].vault_secret_path,
    module.prat_read_registry[*].vault_secret_path,
  )
  vault_protected_secret_paths = setunion(
    var.vault.protected_secret_paths,
    module.prat_common_ci_tasks_goreleaser[*].vault_secret_path,
    module.prat_common_ci_tasks_renovate_bot[*].vault_secret_path,
    module.prat_common_ci_tasks_semantic_release[*].vault_secret_path,
    local.common_ci_tasks.renovate_bot ? [var.common_ci_tasks.renovate_bot_github_token_path] : []
  )
  vault_readwrite_secret_paths = var.vault.readwrite_secret_paths
  # Vault Transit
  vault_transit_key_name = join("-", [var.vault.auth_path, local.project_full_path_safe])
}
