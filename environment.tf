resource "gitlab_project_protected_environment" "environment" {
  for_each = var.archived ? {} : {
    for env, rules in var.protected_environments :
    env => rules
    if rules.deploy_access_levels.access_level != null || length(rules.deploy_access_levels.user_ids) > 0 || length(rules.deploy_access_levels.group_ids) > 0
  }

  project = local.project_id

  environment    = each.key
  approval_rules = each.value.approval_rules

  dynamic "deploy_access_levels" {
    for_each = each.value.deploy_access_levels.access_level != null ? [1] : []

    content {
      access_level = each.value.deploy_access_levels.access_level
    }
  }

  dynamic "deploy_access_levels" {
    for_each = toset(each.value.deploy_access_levels.group_ids)

    content {
      group_id = deploy_access_levels.value
    }
  }

  dynamic "deploy_access_levels" {
    for_each = toset(each.value.deploy_access_levels.user_ids)

    content {
      user_id = deploy_access_levels.value
    }
  }
}
