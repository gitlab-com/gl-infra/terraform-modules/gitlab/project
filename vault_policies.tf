# Policy readonly
data "vault_policy_document" "project_readonly" {
  count = local.vault_enabled ? 1 : 0

  # Terraform short lived session
  rule {
    path         = "auth/token/create"
    capabilities = ["update"]
  }

  # Allow to self lookup token
  rule {
    path         = "auth/token/lookup-self"
    capabilities = ["read"]
  }

  # Project secrets
  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/shared/*"
    capabilities = ["list", "read"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/shared/*"
    capabilities = ["list", "read"]
  }

  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/*"
    capabilities = ["list", "read"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/*"
    capabilities = ["list", "read"]
  }

  # Outputs (usually from Terraform)
  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/outputs/*"
    capabilities = ["list", "read"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/outputs/*"
    capabilities = ["list", "read"]
  }

  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/outputs/*"
    capabilities = ["list", "read"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/outputs/*"
    capabilities = ["list", "read"]
  }

  # Extra secrets
  dynamic "rule" {
    for_each = setunion(local.vault_readonly_secret_paths, local.vault_readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert data/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/data/")
      capabilities = ["list", "read"]
    }
  }
  dynamic "rule" {
    for_each = setunion(local.vault_readonly_secret_paths, local.vault_readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert metadata/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/metadata/")
      capabilities = ["list", "read"]
    }
  }

  # Transit key
  rule {
    path         = "${local.vault_ci_transit_mount_path}/decrypt/${local.vault_transit_key_name}"
    capabilities = ["create", "update"]
  }

  rule {
    path         = "${local.vault_ci_transit_mount_path}/encrypt/+"
    capabilities = ["create", "update"]
  }

  rule {
    path         = "${local.vault_ci_transit_mount_path}/datakey/${local.vault_transit_key_name}"
    capabilities = ["create", "update"]
  }

  rule {
    path         = "${local.vault_ci_transit_mount_path}/hmac/${local.vault_transit_key_name}"
    capabilities = ["create", "update"]
  }

  rule {
    path         = "${local.vault_ci_transit_mount_path}/hmac/${local.vault_transit_key_name}/+"
    capabilities = ["create", "update"]
  }

  rule {
    path         = "${local.vault_ci_transit_mount_path}/sign/${local.vault_transit_key_name}"
    capabilities = ["create", "update"]
  }

  rule {
    path         = "${local.vault_ci_transit_mount_path}/sign/${local.vault_transit_key_name}/+"
    capabilities = ["create", "update"]
  }

  rule {
    path         = "${local.vault_ci_transit_mount_path}/verify/*"
    capabilities = ["create", "update"]
  }
}

resource "vault_policy" "project_readonly" {
  count = local.vault_enabled ? 1 : 0

  name   = local.vault_default_readonly_policy
  policy = data.vault_policy_document.project_readonly[0].hcl
}

# Policy readwrite
data "vault_policy_document" "project_readwrite" {
  count = local.vault_enabled ? 1 : 0

  # Secrets for protected / read/write branches only
  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/protected/shared/*"
    capabilities = ["list", "read"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/protected/shared/*"
    capabilities = ["list", "read"]
  }

  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/protected/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/*"
    capabilities = ["list", "read"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/protected/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/*"
    capabilities = ["list", "read"]
  }

  # Outputs (usually from Terraform)
  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/outputs/*"
    capabilities = ["list", "read", "create", "patch", "update", "delete"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/outputs/*"
    capabilities = ["list", "read", "create", "patch", "update", "delete"]
  }
  rule {
    path         = "${local.vault_ci_path}/delete/${var.vault.auth_path}/${local.project_path_with_namespace}/outputs/*"
    capabilities = ["update"]
  }
  rule {
    path         = "${local.vault_ci_path}/undelete/${var.vault.auth_path}/${local.project_path_with_namespace}/outputs/*"
    capabilities = ["update"]
  }
  rule {
    path         = "${local.vault_ci_path}/destroy/${var.vault.auth_path}/${local.project_path_with_namespace}/outputs/*"
    capabilities = ["update"]
  }

  rule {
    path         = "${local.vault_ci_path}/data/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/outputs/*"
    capabilities = ["list", "read", "create", "patch", "update", "delete"]
  }
  rule {
    path         = "${local.vault_ci_path}/metadata/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/outputs/*"
    capabilities = ["list", "read", "create", "patch", "update", "delete"]
  }
  rule {
    path         = "${local.vault_ci_path}/delete/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/outputs/*"
    capabilities = ["update"]
  }
  rule {
    path         = "${local.vault_ci_path}/undelete/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/outputs/*"
    capabilities = ["update"]
  }
  rule {
    path         = "${local.vault_ci_path}/destroy/${var.vault.auth_path}/${local.project_path_with_namespace}/{{identity.entity.aliases.${local.vault_jwt_auth_backend_accessor}.metadata.environment}}/outputs/*"
    capabilities = ["update"]
  }

  # Extra secrets
  dynamic "rule" {
    for_each = toset(local.vault_protected_secret_paths)
    content {
      # rebuilding the extra path to insert data/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/data/")
      capabilities = ["list", "read"]
    }
  }
  dynamic "rule" {
    for_each = toset(local.vault_protected_secret_paths)
    content {
      # rebuilding the extra path to insert metadata/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/metadata/")
      capabilities = ["list", "read"]
    }
  }
  dynamic "rule" {
    for_each = toset(local.vault_readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert data/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/data/")
      capabilities = ["list", "read", "create", "patch", "update", "delete"]
    }
  }
  dynamic "rule" {
    for_each = toset(local.vault_readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert metadata/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/metadata/")
      capabilities = ["list", "read", "create", "patch", "update", "delete"]
    }
  }
  dynamic "rule" {
    for_each = toset(local.vault_readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert delete/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/delete/")
      capabilities = ["update"]
    }
  }
  dynamic "rule" {
    for_each = toset(local.vault_readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert undelete/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/undelete/")
      capabilities = ["update"]
    }
  }
  dynamic "rule" {
    for_each = toset(local.vault_readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert destroy/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/destroy/")
      capabilities = ["update"]
    }
  }
}

resource "vault_policy" "project_readwrite" {
  count = local.vault_enabled ? 1 : 0

  name   = local.vault_default_readwrite_policy
  policy = data.vault_policy_document.project_readwrite[0].hcl
}

# Environment policies
data "vault_policy_document" "project_env_readonly" {
  for_each = local.vault_enabled ? { for k, v in var.vault.environment_ro_roles : k => v if length(concat(v.readonly_secret_paths, v.readwrite_secret_paths)) > 0 } : {}

  dynamic "rule" {
    for_each = setunion(each.value.readonly_secret_paths, each.value.readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert data/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/data/")
      capabilities = ["list", "read"]
    }
  }
  dynamic "rule" {
    for_each = setunion(each.value.readonly_secret_paths, each.value.readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert metadata/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/metadata/")
      capabilities = ["list", "read"]
    }
  }
}

resource "vault_policy" "project_env_readonly" {
  for_each = local.vault_enabled ? { for k, v in var.vault.environment_ro_roles : k => v if length(concat(v.readonly_secret_paths, v.readwrite_secret_paths)) > 0 } : {}

  name   = format(local.vault_env_readonly_policy, each.key)
  policy = data.vault_policy_document.project_env_readonly[each.key].hcl
}

# Policy readwrite
data "vault_policy_document" "project_env_readwrite" {
  for_each = local.vault_enabled ? { for k, v in var.vault.environment_rw_roles : k => v if length(concat(v.readonly_secret_paths, v.readwrite_secret_paths)) > 0 } : {}

  dynamic "rule" {
    for_each = toset(each.value.readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert data/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/data/")
      capabilities = ["list", "read", "create", "patch", "update", "delete"]
    }
  }
  dynamic "rule" {
    for_each = toset(each.value.readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert metadata/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/metadata/")
      capabilities = ["list", "read", "create", "patch", "update", "delete"]
    }
  }
  dynamic "rule" {
    for_each = toset(each.value.readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert delete/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/delete/")
      capabilities = ["update"]
    }
  }
  dynamic "rule" {
    for_each = toset(each.value.readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert undelete/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/undelete/")
      capabilities = ["update"]
    }
  }
  dynamic "rule" {
    for_each = toset(each.value.readwrite_secret_paths)
    content {
      # rebuilding the extra path to insert destroy/
      path         = replace(rule.value, local.vault_kv_v2_expand_regex, "$1/destroy/")
      capabilities = ["update"]
    }
  }
}

resource "vault_policy" "project_env_readwrite" {
  for_each = local.vault_enabled ? { for k, v in var.vault.environment_rw_roles : k => v if length(concat(v.readonly_secret_paths, v.readwrite_secret_paths)) > 0 } : {}

  name   = format(local.vault_env_readwrite_policy, each.key)
  policy = data.vault_policy_document.project_env_readwrite[each.key].hcl
}
