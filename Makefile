default: docs

README_md = README.md $(wildcard modules/*/README.md)

.PHONY: docs $(README_md) check-docs
.SILENT: $(README_md) check-docs

docs: $(README_md)

$(README_md):
	terraform-docs --config .terraform-docs.yml $$(dirname $@)

check-docs:
	for f in $(README_md); do \
		terraform-docs --output-check $$(dirname $$f); \
	done
