# Vault role variable - protected branches/environments
resource "gitlab_project_variable" "vault_role_environment" {
  for_each = local.vault_enabled ? var.protected_environments : {}

  project = local.project_id

  key = "VAULT_AUTH_ROLE"
  # Environment specific roles take precedence
  value = coalesce(
    lookup(
      lookup(vault_jwt_auth_backend_role.project_role_environment_rw, each.key, {}),
      "role_name",
      local.vault_protected_role
    ),
    var.vault.auth_role_protected
  )

  description = "The role to use when attempting to authenticate to Vault. (IaC managed)"

  raw               = true
  protected         = true
  masked            = false
  environment_scope = each.key
}

# Vault role variable - default readonly
resource "gitlab_project_variable" "vault_role_readonly" {
  count = local.vault_enabled ? 1 : 0

  project = local.project_id

  key   = "VAULT_AUTH_ROLE"
  value = format("%s$${VAULT_AUTH_ROLE_SUFFIX}", coalesce(var.vault.auth_role_readonly, var.vault.non_protected_readonly ? local.vault_readonly_role : local.vault_protected_role))

  description = "The role to use when attempting to authenticate to Vault. (IaC managed)"

  raw               = false
  protected         = false
  masked            = false
  environment_scope = "*"
}

# Vault role suffix variable - only set in protected branches/environments and
# expanded in VAULT_AUTH_ROLE above to update it to the readwrite role
# automatically
resource "gitlab_project_variable" "vault_role_suffix" {
  count = local.vault_enabled && var.vault.non_protected_readonly ? 1 : 0

  project = local.project_id

  key   = "VAULT_AUTH_ROLE_SUFFIX"
  value = var.vault.auth_role_protected_suffix

  description = "Suffix to use the read/write role in protected branches. (IaC managed)"

  raw               = true
  protected         = true
  masked            = false
  environment_scope = "*"
}

# Vault secrets path variable
resource "gitlab_project_variable" "vault_secrets_path" {
  count = local.vault_enabled ? 1 : 0

  project = local.project_id

  key   = "VAULT_SECRETS_PATH"
  value = local.vault_secrets_path

  description = "Path of this project's secrets in Vault. (IaC managed)"

  raw               = true
  protected         = false
  masked            = false
  environment_scope = "*"
}

# Vault transit key name
resource "gitlab_project_variable" "vault_transit_key_name" {
  count = local.vault_enabled ? 1 : 0

  project = local.project_id

  key   = "VAULT_TRANSIT_KEY_NAME"
  value = local.vault_transit_key_name

  description = "Project's key for the CI transit secret engine. (IaC managed)"

  raw               = true
  protected         = false
  masked            = false
  environment_scope = "*"
}
