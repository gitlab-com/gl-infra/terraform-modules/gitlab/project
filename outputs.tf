output "id" {
  description = "ID of the project."
  value       = local.project_id
}

output "path_with_namespace" {
  description = "Full path of the project."
  value       = local.project_path_with_namespace
}

output "prat_common_ci_tasks_semantic_release" {
  description = "All outputs from the semantic release project token"
  value       = local.common_ci_tasks.semantic_release ? module.prat_common_ci_tasks_semantic_release[0] : null
}

output "prat_common_ci_tasks_danger" {
  description = "All outputs from the danger project token"
  value       = local.common_ci_tasks.danger ? module.prat_common_ci_tasks_danger[0] : null
}

output "prat_common_ci_tasks_autolabels" {
  description = "All outputs from the autolabels project token"
  value       = local.common_ci_tasks.autolabels ? module.prat_common_ci_tasks_autolabels[0] : null
}

output "repo_http_url" {
  description = "HTTPS URL for the repository."
  value       = var.use_existing_project ? data.gitlab_project.project[0].http_url_to_repo : gitlab_project.project[0].http_url_to_repo
}

output "repo_ssh_url" {
  description = "SSH URL for the repository."
  value       = var.use_existing_project ? data.gitlab_project.project[0].ssh_url_to_repo : gitlab_project.project[0].ssh_url_to_repo
}

output "vault_secrets_path" {
  description = "Full path of the Vault secrets for the project."
  value       = local.vault_secrets_path
}

output "web_url" {
  description = "URL of the project."
  value       = var.use_existing_project ? data.gitlab_project.project[0].web_url : gitlab_project.project[0].web_url
}
