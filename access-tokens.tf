# A token that can be used for read-only access to the project registry
module "prat_read_registry" {
  count = var.registry_read_token ? 1 : 0

  source = "./modules/access-token"

  project_id   = local.project_id
  name         = "read-registry"
  access_level = "developer"
  scopes       = ["read_registry"]

  vault_kv_secret = {
    store        = local.vault_enabled
    mount        = local.vault_ci_path
    auth_path    = var.vault.auth_path
    project_path = local.project_path_with_namespace
    name         = "read-registry"
  }
}
