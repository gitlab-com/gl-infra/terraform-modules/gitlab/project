# Protected role
resource "vault_jwt_auth_backend_role" "project_role_protected" {
  count = local.vault_enabled && var.vault.auth_role_protected == null ? 1 : 0

  backend = var.vault.auth_path

  role_name = local.vault_protected_role
  role_type = "jwt"

  user_claim     = "job_id"
  claim_mappings = local.vault_ci_jwt_claim_mappings

  bound_audiences = var.vault.auth_role_bound_audiences

  # Repo details
  # https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
  bound_claims_type = "string"
  bound_claims = merge(
    { project_id = local.project_id },
    # Allow readwrite only if branch/ref is protected
    var.vault.non_protected_readonly ? { ref_protected = "true" } : {}
  )

  token_policies = setunion(
    [local.vault_default_readonly_policy],
    [local.vault_default_readwrite_policy],
    var.vault.extra_readonly_policies,
    var.vault.extra_protected_policies,
  )

  token_ttl     = var.vault.token_ttl
  token_max_ttl = var.vault.token_max_ttl
}

# Default role
resource "vault_jwt_auth_backend_role" "project_role_readonly" {
  count = local.vault_enabled && var.vault.auth_role_readonly == null ? 1 : 0

  backend = var.vault.auth_path

  role_name = local.vault_readonly_role
  role_type = "jwt"

  user_claim     = "job_id"
  claim_mappings = local.vault_ci_jwt_claim_mappings

  bound_audiences = var.vault.auth_role_bound_audiences

  # Repo details
  # https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
  bound_claims_type = "string"
  bound_claims = {
    project_id = local.project_id
    # ref_protected = "false"
  }

  token_policies = setunion(
    [local.vault_default_readonly_policy],
    var.vault.extra_readonly_policies,
  )

  token_ttl     = var.vault.token_ttl
  token_max_ttl = var.vault.token_max_ttl
}

# Environment read-only role
resource "vault_jwt_auth_backend_role" "project_role_environment_ro" {
  for_each = local.vault_enabled ? var.vault.environment_ro_roles : {}

  backend = var.vault.auth_path

  role_name = format(local.vault_readonly_env_role, each.key)
  role_type = "jwt"

  user_claim     = "job_id"
  claim_mappings = local.vault_ci_jwt_claim_mappings

  bound_audiences = var.vault.auth_role_bound_audiences

  # Repo details
  # https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
  bound_claims_type = "string"
  bound_claims = merge(
    {
      project_id  = local.project_id
      environment = each.key
    },
    each.value.protected ? { environment_protected = "true" } : {},
  )

  token_policies = setunion(
    [local.vault_default_readonly_policy],
    var.vault.extra_readonly_policies,
    compact([lookup(lookup(vault_policy.project_env_readonly, each.key, {}), "name", null)]),
  )

  token_ttl     = var.vault.token_ttl
  token_max_ttl = var.vault.token_max_ttl
}

# Environment read-write role
resource "vault_jwt_auth_backend_role" "project_role_environment_rw" {
  for_each = local.vault_enabled ? var.vault.environment_rw_roles : {}

  backend = var.vault.auth_path

  role_name = format(local.vault_readwrite_env_role, each.key)
  role_type = "jwt"

  user_claim     = "job_id"
  claim_mappings = local.vault_ci_jwt_claim_mappings

  bound_audiences = var.vault.auth_role_bound_audiences

  # Repo details
  # https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
  bound_claims_type = "string"
  bound_claims = merge(
    {
      project_id    = local.project_id
      ref_protected = "true"
      environment   = each.key
    },
    each.value.protected ? { environment_protected = "true" } : {},
  )

  token_policies = setunion(
    [local.vault_default_readonly_policy],
    [local.vault_default_readwrite_policy],
    var.vault.extra_readonly_policies,
    var.vault.extra_protected_policies,
    compact([lookup(lookup(vault_policy.project_env_readonly, each.key, {}), "name", null)]),
    compact([lookup(lookup(vault_policy.project_env_readwrite, each.key, {}), "name", null)]),
  )

  token_ttl     = var.vault.token_ttl
  token_max_ttl = var.vault.token_max_ttl
}

# Placeholder file, allows users to see the secrets path
resource "vault_kv_secret_v2" "placeholder" {
  count = local.vault_enabled && !var.archived ? 1 : 0

  mount = local.vault_ci_path
  name  = "${local.vault_secrets_path}/${local.vault_ci_placeholder_file}"

  data_json           = "{}"
  delete_all_versions = true
}
