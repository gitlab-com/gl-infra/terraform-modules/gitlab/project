data "vault_auth_backend" "jwt" {
  count = local.vault_enabled ? 1 : 0
  path  = var.vault.auth_path
}

data "gitlab_group" "group" {
  count = var.use_existing_project ? 1 : 0

  group_id = var.group_id
}

data "gitlab_project" "project" {
  count = var.use_existing_project ? 1 : 0

  path_with_namespace = join("/", [data.gitlab_group.group[0].full_path, var.path])
}
