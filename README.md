# GitLab Project Terraform Module

## What is this?

This module provisions a GitLab project.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 17.3.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.0 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | >= 3.9 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 17.3.0 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 3.0 |
| <a name="provider_vault"></a> [vault](#provider\_vault) | >= 3.9 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_prat_common_ci_tasks_autolabels"></a> [prat\_common\_ci\_tasks\_autolabels](#module\_prat\_common\_ci\_tasks\_autolabels) | ./modules/access-token | n/a |
| <a name="module_prat_common_ci_tasks_danger"></a> [prat\_common\_ci\_tasks\_danger](#module\_prat\_common\_ci\_tasks\_danger) | ./modules/access-token | n/a |
| <a name="module_prat_common_ci_tasks_goreleaser"></a> [prat\_common\_ci\_tasks\_goreleaser](#module\_prat\_common\_ci\_tasks\_goreleaser) | ./modules/access-token | n/a |
| <a name="module_prat_common_ci_tasks_mirroring"></a> [prat\_common\_ci\_tasks\_mirroring](#module\_prat\_common\_ci\_tasks\_mirroring) | ./modules/access-token | n/a |
| <a name="module_prat_common_ci_tasks_renovate_bot"></a> [prat\_common\_ci\_tasks\_renovate\_bot](#module\_prat\_common\_ci\_tasks\_renovate\_bot) | ./modules/access-token | n/a |
| <a name="module_prat_common_ci_tasks_semantic_release"></a> [prat\_common\_ci\_tasks\_semantic\_release](#module\_prat\_common\_ci\_tasks\_semantic\_release) | ./modules/access-token | n/a |
| <a name="module_prat_read_registry"></a> [prat\_read\_registry](#module\_prat\_read\_registry) | ./modules/access-token | n/a |

## Resources

| Name | Type |
|------|------|
| [gitlab_branch_protection.branch](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection) | resource |
| [gitlab_pipeline_schedule.common_ci_tasks_renovate_bot](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule) | resource |
| [gitlab_pipeline_schedule.common_ci_tasks_renovate_bot_immediate](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule) | resource |
| [gitlab_pipeline_schedule_variable.common_ci_tasks_renovate_bot_immediate](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule_variable) | resource |
| [gitlab_pipeline_schedule_variable.common_ci_tasks_renovate_bot_scheduled](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule_variable) | resource |
| [gitlab_project.project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project) | resource |
| [gitlab_project_approval_rule.approval](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_approval_rule) | resource |
| [gitlab_project_job_token_scope.non_authoritative](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_job_token_scope) | resource |
| [gitlab_project_job_token_scopes.authoritative](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_job_token_scopes) | resource |
| [gitlab_project_level_mr_approvals.mr](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_level_mr_approvals) | resource |
| [gitlab_project_membership.user](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_membership) | resource |
| [gitlab_project_mirror.mirror](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_mirror) | resource |
| [gitlab_project_protected_environment.environment](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_protected_environment) | resource |
| [gitlab_project_share_group.project_share_group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_share_group) | resource |
| [gitlab_project_variable.common_ci_tasks_autolabels_enabled](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.vault_role_environment](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.vault_role_readonly](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.vault_role_suffix](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.vault_secrets_path](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.vault_transit_key_name](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_tag_protection.common_ci_tasks_renovate_bot_protected_tags](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/tag_protection) | resource |
| [gitlab_tag_protection.tag](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/tag_protection) | resource |
| [random_integer.common_ci_tasks_renovate_bot_hour](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |
| [random_integer.common_ci_tasks_renovate_bot_minute](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |
| [vault_jwt_auth_backend_role.project_role_environment_ro](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend_role) | resource |
| [vault_jwt_auth_backend_role.project_role_environment_rw](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend_role) | resource |
| [vault_jwt_auth_backend_role.project_role_protected](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend_role) | resource |
| [vault_jwt_auth_backend_role.project_role_readonly](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend_role) | resource |
| [vault_kv_secret_v2.placeholder](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |
| [vault_policy.project_env_readonly](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.project_env_readwrite](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.project_readonly](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.project_readwrite](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_transit_secret_backend_key.transit-key](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/transit_secret_backend_key) | resource |
| [gitlab_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |
| [gitlab_project.project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |
| [vault_auth_backend.jwt](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/auth_backend) | data source |
| [vault_policy_document.project_env_readonly](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.project_env_readwrite](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.project_readonly](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.project_readwrite](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_approval_rules"></a> [approval\_rules](#input\_approval\_rules) | Project-level approval rules.<br/><br/>Example:<br/>{<br/>  "my\_rule" = {<br/>    approvals\_required = 2<br/>    user\_ids           = [2, 3]<br/>    group\_ids          = [4]<br/>  }<br/>} | <pre>map(object({<br/>    approvals_required = number<br/>    user_ids           = optional(list(number))<br/>    group_ids          = optional(list(number))<br/>  }))</pre> | `{}` | no |
| <a name="input_archived"></a> [archived](#input\_archived) | Archive the project. | `bool` | `false` | no |
| <a name="input_branch_protections"></a> [branch\_protections](#input\_branch\_protections) | Protect repository branches.<br/><br/>Example:<br/>{<br/>  my\_branch = {<br/>    merge\_access\_level = "maintainer"<br/>    push\_access\_level  = "no one"<br/>    allowed\_to\_push = {<br/>      user\_ids = [42, 123]<br/>    }<br/>  }<br/>}<br/><br/>Valid access level values: "no one", "developer", or "maintainer" | <pre>map(object({<br/>    code_owner_approval_required = optional(bool)<br/>    merge_access_level           = optional(string, "maintainer")<br/>    push_access_level            = optional(string, "no one")<br/>    unprotect_access_level       = optional(string)<br/>    allow_force_push             = optional(bool)<br/>    allowed_to_merge = optional(object({<br/>      user_ids  = optional(list(number), [])<br/>      group_ids = optional(list(number), [])<br/>    }), {})<br/>    allowed_to_push = optional(object({<br/>      user_ids  = optional(list(number), [])<br/>      group_ids = optional(list(number), [])<br/>    }), {})<br/>    allowed_to_unprotect = optional(object({<br/>      user_ids  = optional(list(number), [])<br/>      group_ids = optional(list(number), [])<br/>    }), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_common_ci_tasks"></a> [common\_ci\_tasks](#input\_common\_ci\_tasks) | Integration settings for [`common-ci-tasks`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks). | <pre>object({<br/>    enabled                            = optional(bool, false)<br/>    goreleaser                         = optional(bool, true)<br/>    mirroring                          = optional(bool, false)<br/>    renovate_bot                       = optional(bool, true)<br/>    renovate_bot_github_token_path     = optional(string, "shared/renovate/github")<br/>    renovate_bot_schedule              = optional(string)<br/>    semantic_release                   = optional(bool, true)<br/>    danger                             = optional(bool, false)<br/>    republishes_truncated_version_tags = optional(bool, false)<br/>    autolabels                         = optional(bool, true)<br/>  })</pre> | `{}` | no |
| <a name="input_container_registry_access_level"></a> [container\_registry\_access\_level](#input\_container\_registry\_access\_level) | Visibility of container registry for the project. Can be `disabled`, `private`, or `enabled`. | `string` | `"enabled"` | no |
| <a name="input_default_branch"></a> [default\_branch](#input\_default\_branch) | The default branch name, unmanaged if not set. | `string` | `null` | no |
| <a name="input_description"></a> [description](#input\_description) | Short project description. | `string` | `null` | no |
| <a name="input_ee"></a> [ee](#input\_ee) | Enable GitLab Enterprise features. | `bool` | `true` | no |
| <a name="input_forked_from_project_id"></a> [forked\_from\_project\_id](#input\_forked\_from\_project\_id) | The ID of the project to fork. | `number` | `null` | no |
| <a name="input_group_id"></a> [group\_id](#input\_group\_id) | Group ID of the new project. | `number` | n/a | yes |
| <a name="input_initialize_with_readme"></a> [initialize\_with\_readme](#input\_initialize\_with\_readme) | Whether to create a Git repository with just a README.md file. | `bool` | `null` | no |
| <a name="input_issues"></a> [issues](#input\_issues) | Issues settings for the project. | <pre>object({<br/>    access_level = optional(string, "enabled")<br/>    template     = optional(string)<br/>  })</pre> | `{}` | no |
| <a name="input_job_token_scopes"></a> [job\_token\_scopes](#input\_job\_token\_scopes) | Job token scopes. | <pre>object({<br/>    authoritative      = optional(bool, false)<br/>    target_project_ids = optional(set(number), [])<br/>  })</pre> | `{}` | no |
| <a name="input_lfs_enabled"></a> [lfs\_enabled](#input\_lfs\_enabled) | Enable/disable Large File Storage (LFS) for the project. | `bool` | `true` | no |
| <a name="input_members"></a> [members](#input\_members) | Project user memberships.<br/><br/>Example:<br/>{<br/>  <user\_id> = {<br/>    access\_level = "developer"<br/>  }<br/>}<br/>Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner" | <pre>map(object({<br/>    access_level = string<br/>  }))</pre> | `{}` | no |
| <a name="input_merge_requests"></a> [merge\_requests](#input\_merge\_requests) | Merge Requests settings for the project. | <pre>object({<br/>    access_level                                     = optional(string, "enabled")<br/>    approvals_before_merge                           = optional(number, 0)<br/>    author_approval                                  = optional(bool, false)<br/>    default_target_self                              = optional(bool, true)<br/>    disable_committers_approval                      = optional(bool, true)<br/>    disable_overriding_approvers_per_merge_request   = optional(bool, true)<br/>    merge_method                                     = optional(string, "merge")<br/>    merge_pipelines_enabled                          = optional(bool, false)<br/>    merge_trains_enabled                             = optional(bool, false)<br/>    only_allow_merge_if_all_discussions_are_resolved = optional(bool, true)<br/>    allow_merge_on_skipped_pipeline                  = optional(bool, false)<br/>    only_allow_merge_if_pipeline_succeeds            = optional(bool, true)<br/>    remove_source_branch_after_merge                 = optional(bool, true)<br/>    require_password_to_approve                      = optional(bool, false)<br/>    reset_approvals_on_push                          = optional(bool, true)<br/>    selective_code_owner_removals                    = optional(bool, false)<br/>    squash_option                                    = optional(string, "default_off")<br/>    squash_commit_template                           = optional(string)<br/>    commit_template                                  = optional(string)<br/>    template                                         = optional(string)<br/>  })</pre> | `{}` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the new project. Equals path if not provided. | `string` | `null` | no |
| <a name="input_packages_enabled"></a> [packages\_enabled](#input\_packages\_enabled) | Enable packages repository for the project. | `bool` | `true` | no |
| <a name="input_pages_access_level"></a> [pages\_access\_level](#input\_pages\_access\_level) | Visibility of GitLab Pages for the project. Can be `disabled`, `enabled`, `private`, or `public`. | `string` | `null` | no |
| <a name="input_path"></a> [path](#input\_path) | Repository name for new project. | `string` | n/a | yes |
| <a name="input_pipelines"></a> [pipelines](#input\_pipelines) | Pipelines settings for the project. | <pre>object({<br/>    access_level                  = optional(string, "enabled")<br/>    auto_cancel_pending_pipelines = optional(string)<br/>    ci_config_path                = optional(string)<br/>    ci_default_git_depth          = optional(number)<br/>    ci_forward_deployment_enabled = optional(bool)<br/>    ci_separated_caches           = optional(bool)<br/>    build_timeout                 = optional(number)<br/>    keep_latest_artifact          = optional(bool)<br/>    public_jobs                   = optional(bool)<br/>  })</pre> | `{}` | no |
| <a name="input_protected_environments"></a> [protected\_environments](#input\_protected\_environments) | Protected environments rules, also adds protected variables (Vault roles, etc.).<br/><br/>Example:<br/>{<br/>  "my\_environment" = {<br/>    approval\_rules = {<br/>      required\_approvals = 1<br/>    }<br/>    deploy\_access\_levels = {<br/>      access\_level = "maintainer"<br/>      group\_ids    = [4]<br/>      user\_ids     = [2, 3]<br/>    }<br/>  }<br/>} | <pre>map(object({<br/>    approval_rules = optional(list(object({<br/>      access_level           = optional(string)<br/>      group_id               = optional(number)<br/>      group_inheritance_type = optional(number)<br/>      required_approvals     = optional(number)<br/>      user_id                = optional(number)<br/>      })<br/>    ))<br/>    deploy_access_levels = optional(object({<br/>      access_level = optional(string)<br/>      group_ids    = optional(list(number), [])<br/>      user_ids     = optional(list(number), [])<br/>    }))<br/>  }))</pre> | `{}` | no |
| <a name="input_pull_mirror"></a> [pull\_mirror](#input\_pull\_mirror) | Pull mirror settings for this project. | <pre>object({<br/>    enabled                      = optional(bool, false)<br/>    url                          = optional(string)<br/>    trigger_builds               = optional(bool)<br/>    overwrites_diverged_branches = optional(bool)<br/>  })</pre> | `{}` | no |
| <a name="input_push_mirrors"></a> [push\_mirrors](#input\_push\_mirrors) | Push mirrors settings for this project. | <pre>list(object({<br/>    enabled                 = optional(bool, false)<br/>    url                     = optional(string, "")<br/>    only_protected_branches = optional(bool)<br/>    keep_divergent_refs     = optional(bool)<br/>  }))</pre> | `[]` | no |
| <a name="input_registry_read_token"></a> [registry\_read\_token](#input\_registry\_read\_token) | Generate a registry read token and put it into Vault. | `bool` | `false` | no |
| <a name="input_request_access_enabled"></a> [request\_access\_enabled](#input\_request\_access\_enabled) | Allow users to request member access. | `bool` | `false` | no |
| <a name="input_share_groups"></a> [share\_groups](#input\_share\_groups) | Share the project with other groups.<br/>Example:<br/>{<br/>  <name> = {<br/>    group\_id     = 12345<br/>    group\_access = "developer"<br/>  }<br/>}<br/>Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner" | <pre>map(object({<br/>    group_id     = number<br/>    group_access = string<br/>  }))</pre> | `{}` | no |
| <a name="input_shared_runners_enabled"></a> [shared\_runners\_enabled](#input\_shared\_runners\_enabled) | Enable shared runners for the project. | `bool` | `true` | no |
| <a name="input_snippets_access_level"></a> [snippets\_access\_level](#input\_snippets\_access\_level) | Visibility of the snippets for the project. Can be `disabled`, `private`, or `enabled`. | `string` | `"disabled"` | no |
| <a name="input_tag_protections"></a> [tag\_protections](#input\_tag\_protections) | Protect repository tags.<br/><br/>Example:<br/>{<br/>  "my\_tag" = {<br/>    create\_access\_level = "developer"<br/>  }<br/>}<br/>Valid values:<br/>"no one":     gitlab.NoPermissions,<br/>"guest":      gitlab.GuestPermissions,<br/>"reporter":   gitlab.ReporterPermissions,<br/>"developer":  gitlab.DeveloperPermissions,<br/>"maintainer": gitlab.MaintainerPermissions,<br/>"owner":      gitlab.OwnerPermission, | <pre>map(object({<br/>    create_access_level = string<br/>    allowed_to_create = optional(object({<br/>      access_level = optional(string)<br/>      user_ids     = optional(list(number), [])<br/>      group_ids    = optional(list(number), [])<br/>    }), {})<br/>  }))</pre> | <pre>{<br/>  "*": {<br/>    "create_access_level": "maintainer"<br/>  }<br/>}</pre> | no |
| <a name="input_template"></a> [template](#input\_template) | Built-in or custom project template settings. | <pre>object({<br/>    name                = optional(string)<br/>    project_id          = optional(number)<br/>    use_custom_template = optional(bool, false)<br/>  })</pre> | `{}` | no |
| <a name="input_topics"></a> [topics](#input\_topics) | The list of topics for a project. | `set(string)` | `[]` | no |
| <a name="input_use_existing_project"></a> [use\_existing\_project](#input\_use\_existing\_project) | Enable this to use an existing project without modifying its current settings. | `bool` | `false` | no |
| <a name="input_vault"></a> [vault](#input\_vault) | Vault settings for the project. | <pre>object({<br/>    enabled                    = optional(bool, false)<br/>    auth_path                  = optional(string, "")<br/>    auth_role_bound_audiences  = optional(list(string), ["https://vault.gitlab.net"])<br/>    auth_role_protected        = optional(string)<br/>    auth_role_protected_suffix = optional(string, "-rw")<br/>    auth_role_readonly         = optional(string)<br/>    # Environment roles, ro == unprotected branch, rw == protected branch<br/>    environment_ro_roles = optional(map(object({<br/>      readonly_secret_paths  = optional(list(string), [])<br/>      readwrite_secret_paths = optional(list(string), [])<br/>      protected              = optional(bool, true)<br/>    })), {})<br/>    environment_rw_roles = optional(map(object({<br/>      readonly_secret_paths  = optional(list(string), [])<br/>      readwrite_secret_paths = optional(list(string), [])<br/>      protected              = optional(bool, true)<br/>    })), {})<br/>    # Non protected branches/environments will get a readonly role<br/>    non_protected_readonly = optional(bool, true)<br/>    # Extra CI secret paths the project should have readonly access to from protected branches/environments only, relative to K/V backend<br/>    protected_secret_paths = optional(list(string), [])<br/>    # Extra CI secret paths the project should have readonly access to, relative to K/V backend<br/>    readonly_secret_paths = optional(list(string), [])<br/>    # Extra CI secret paths the project should have read/write access to, relative to K/V backend<br/>    readwrite_secret_paths = optional(list(string), [])<br/>    # Extra policies for the protected role<br/>    extra_protected_policies = optional(list(string), [])<br/>    # Extra policies for the readonly role<br/>    extra_readonly_policies = optional(list(string), [])<br/>    # Default TTL<br/>    token_ttl = optional(number, 3600) # 1 hour<br/>    # Max TTL<br/>    token_max_ttl = optional(number, 10800) # 3 hours<br/>  })</pre> | `{}` | no |
| <a name="input_visibility_level"></a> [visibility\_level](#input\_visibility\_level) | The project’s visibility. Can be `private`, `internal`, or `public`. | `string` | `"public"` | no |
| <a name="input_wiki_access_level"></a> [wiki\_access\_level](#input\_wiki\_access\_level) | Visibility of the wiki for the project. Can be `disabled`, `private`, or `enabled`. | `string` | `"disabled"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | ID of the project. |
| <a name="output_path_with_namespace"></a> [path\_with\_namespace](#output\_path\_with\_namespace) | Full path of the project. |
| <a name="output_prat_common_ci_tasks_autolabels"></a> [prat\_common\_ci\_tasks\_autolabels](#output\_prat\_common\_ci\_tasks\_autolabels) | All outputs from the autolabels project token |
| <a name="output_prat_common_ci_tasks_danger"></a> [prat\_common\_ci\_tasks\_danger](#output\_prat\_common\_ci\_tasks\_danger) | All outputs from the danger project token |
| <a name="output_prat_common_ci_tasks_semantic_release"></a> [prat\_common\_ci\_tasks\_semantic\_release](#output\_prat\_common\_ci\_tasks\_semantic\_release) | All outputs from the semantic release project token |
| <a name="output_repo_http_url"></a> [repo\_http\_url](#output\_repo\_http\_url) | HTTPS URL for the repository. |
| <a name="output_repo_ssh_url"></a> [repo\_ssh\_url](#output\_repo\_ssh\_url) | SSH URL for the repository. |
| <a name="output_vault_secrets_path"></a> [vault\_secrets\_path](#output\_vault\_secrets\_path) | Full path of the Vault secrets for the project. |
| <a name="output_web_url"></a> [web\_url](#output\_web\_url) | URL of the project. |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
