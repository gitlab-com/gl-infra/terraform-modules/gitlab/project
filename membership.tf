resource "gitlab_project_membership" "user" {
  for_each = var.members

  project = local.project_id

  user_id      = each.key
  access_level = each.value.access_level
}

resource "gitlab_project_share_group" "project_share_group" {
  for_each = var.share_groups

  project = local.project_id

  group_id     = each.value.group_id
  group_access = each.value.group_access
}
