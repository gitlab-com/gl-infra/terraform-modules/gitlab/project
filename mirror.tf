resource "gitlab_project_mirror" "mirror" {
  for_each = !var.archived ? {
    for mirror in var.push_mirrors :
    nonsensitive(uuidv5("url", mirror.url)) => mirror
  } : {}

  project                 = local.project_id
  enabled                 = each.value.enabled
  url                     = each.value.url
  only_protected_branches = each.value.only_protected_branches
  keep_divergent_refs     = each.value.keep_divergent_refs
}
