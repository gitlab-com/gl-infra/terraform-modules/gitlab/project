resource "gitlab_project_job_token_scopes" "authoritative" {
  count = var.job_token_scopes.authoritative ? 1 : 0

  project            = local.project_id
  target_project_ids = var.job_token_scopes.target_project_ids
}

resource "gitlab_project_job_token_scope" "non_authoritative" {
  for_each = var.job_token_scopes.authoritative ? {} : { for id in var.job_token_scopes.target_project_ids : tostring(id) => id }

  project           = local.project_id
  target_project_id = each.value
}
