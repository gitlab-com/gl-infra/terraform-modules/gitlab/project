# MR approval rules
resource "gitlab_project_approval_rule" "approval" {
  for_each = var.ee && !var.archived && !var.use_existing_project ? var.approval_rules : {}

  project = local.project_id

  name               = each.key
  approvals_required = each.value.approvals_required
  user_ids           = each.value.user_ids
  group_ids          = each.value.group_ids
}

# MR approval settings
resource "gitlab_project_level_mr_approvals" "mr" {
  count = var.ee && !var.archived && !var.use_existing_project ? 1 : 0

  project = local.project_id

  disable_overriding_approvers_per_merge_request = var.merge_requests.disable_overriding_approvers_per_merge_request
  merge_requests_author_approval                 = var.merge_requests.author_approval
  merge_requests_disable_committers_approval     = var.merge_requests.disable_committers_approval
  require_password_to_approve                    = var.merge_requests.require_password_to_approve
  reset_approvals_on_push                        = var.merge_requests.reset_approvals_on_push
  selective_code_owner_removals                  = var.merge_requests.selective_code_owner_removals
}
