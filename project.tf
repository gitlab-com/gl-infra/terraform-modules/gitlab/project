resource "gitlab_project" "project" {
  count = var.use_existing_project ? 0 : 1

  name        = coalesce(var.name, var.path)
  path        = var.path
  description = var.description
  topics      = var.topics

  archive_on_destroy = true
  archived           = var.archived

  default_branch         = var.default_branch
  forked_from_project_id = var.forked_from_project_id
  initialize_with_readme = var.forked_from_project_id != null ? null : var.initialize_with_readme
  mr_default_target_self = var.forked_from_project_id != null ? var.merge_requests.default_target_self : null

  namespace_id           = var.group_id
  visibility_level       = var.visibility_level
  request_access_enabled = var.request_access_enabled

  lfs_enabled            = var.lfs_enabled
  packages_enabled       = var.packages_enabled
  shared_runners_enabled = var.shared_runners_enabled

  container_registry_access_level = var.container_registry_access_level
  pages_access_level              = var.pages_access_level
  snippets_access_level           = var.snippets_access_level
  wiki_access_level               = var.wiki_access_level

  auto_cancel_pending_pipelines = var.pipelines.auto_cancel_pending_pipelines
  build_timeout                 = var.pipelines.build_timeout
  builds_access_level           = var.pipelines.access_level
  ci_config_path                = var.pipelines.ci_config_path
  ci_default_git_depth          = var.pipelines.ci_default_git_depth
  ci_forward_deployment_enabled = var.pipelines.ci_forward_deployment_enabled
  ci_separated_caches           = var.pipelines.ci_separated_caches
  keep_latest_artifact          = var.pipelines.keep_latest_artifact
  public_jobs                   = var.pipelines.public_jobs

  issues_access_level = var.issues.access_level
  issues_template     = var.issues.template

  merge_commit_template                            = var.merge_requests.commit_template
  merge_requests_access_level                      = var.merge_requests.access_level
  merge_requests_template                          = var.merge_requests.template
  merge_method                                     = var.merge_requests.merge_method
  merge_pipelines_enabled                          = var.merge_requests.merge_pipelines_enabled
  merge_trains_enabled                             = var.merge_requests.merge_pipelines_enabled && var.merge_requests.merge_trains_enabled
  allow_merge_on_skipped_pipeline                  = var.merge_requests.allow_merge_on_skipped_pipeline
  approvals_before_merge                           = var.merge_requests.approvals_before_merge
  only_allow_merge_if_all_discussions_are_resolved = var.merge_requests.only_allow_merge_if_all_discussions_are_resolved
  only_allow_merge_if_pipeline_succeeds            = var.merge_requests.only_allow_merge_if_pipeline_succeeds
  remove_source_branch_after_merge                 = var.merge_requests.remove_source_branch_after_merge
  squash_commit_template                           = var.merge_requests.squash_commit_template
  squash_option                                    = var.merge_requests.squash_option

  mirror                              = var.pull_mirror.enabled ? var.pull_mirror.enabled : null
  import_url                          = var.pull_mirror.enabled ? var.pull_mirror.url : null
  mirror_trigger_builds               = var.pull_mirror.enabled ? var.pull_mirror.trigger_builds : null
  mirror_overwrites_diverged_branches = var.pull_mirror.enabled ? var.pull_mirror.overwrites_diverged_branches : null

  template_name       = var.template.name
  template_project_id = var.template.project_id
  use_custom_template = var.template.use_custom_template ? true : null

  lifecycle {
    ignore_changes = [
      template_name,
      template_project_id,
    ]
  }
}
