resource "vault_transit_secret_backend_key" "transit-key" {
  count = local.vault_enabled ? 1 : 0

  backend = local.vault_ci_transit_mount_path
  name    = local.vault_transit_key_name

  // Autorotate key every 3 months
  auto_rotate_period = 90 * 24 * 3600
  deletion_allowed   = true
  type               = "aes256-gcm96"
}
