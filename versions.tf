terraform {
  required_version = ">= 1.4"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 17.3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.0"
    }
    vault = {
      source  = "hashicorp/vault"
      version = ">= 3.9"
    }
  }
}
