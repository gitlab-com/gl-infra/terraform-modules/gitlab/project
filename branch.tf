resource "gitlab_branch_protection" "branch" {
  for_each = var.archived ? {} : var.branch_protections

  project = local.project_id

  branch = each.key

  code_owner_approval_required = var.ee ? each.value.code_owner_approval_required : null

  push_access_level      = each.value.push_access_level
  merge_access_level     = each.value.merge_access_level
  unprotect_access_level = var.ee ? each.value.unprotect_access_level : null
  allow_force_push       = each.value.allow_force_push

  dynamic "allowed_to_merge" {
    for_each = var.ee ? each.value.allowed_to_merge.user_ids : []
    content { user_id = allowed_to_merge.value }
  }

  dynamic "allowed_to_merge" {
    for_each = var.ee ? each.value.allowed_to_merge.group_ids : []
    content { group_id = allowed_to_merge.value }
  }

  dynamic "allowed_to_push" {
    for_each = var.ee ? each.value.allowed_to_push.user_ids : []
    content { user_id = allowed_to_push.value }
  }

  dynamic "allowed_to_push" {
    for_each = var.ee ? each.value.allowed_to_push.group_ids : []
    content { group_id = allowed_to_push.value }
  }

  dynamic "allowed_to_unprotect" {
    for_each = var.ee ? each.value.allowed_to_unprotect.user_ids : []
    content { user_id = allowed_to_unprotect.value }
  }

  dynamic "allowed_to_unprotect" {
    for_each = var.ee ? each.value.allowed_to_unprotect.group_ids : []
    content { group_id = allowed_to_unprotect.value }
  }
}
