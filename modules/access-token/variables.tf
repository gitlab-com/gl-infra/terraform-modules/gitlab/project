variable "project_id" {
  type        = number
  description = "ID of the project to create the Project Access Token in."
}

variable "name" {
  type        = string
  description = "Name of the Project Access Token."
}

variable "access_level" {
  type        = string
  description = "Access level of the Project Access Token."

  validation {
    condition     = contains(["guest", "reporter", "developer", "maintainer", "owner"], var.access_level)
    error_message = "The access_level value must be \"guest\", \"reporter\", \"developer\", \"maintainer\", or \"owner\"."
  }
}

variable "scopes" {
  type        = set(string)
  description = "Scopes of the Project Access Token."

  validation {
    condition     = setintersection(["api", "read_api", "read_repository", "write_repository", "read_registry", "write_registry"], var.scopes) == var.scopes
    error_message = "The access_level values must be \"api\", \"read_api\", \"read_repository\", \"write_repository\", \"read_registry\", \"write_registry\"."
  }
}

variable "expires_after_days" {
  type        = number
  description = "Number of days before the Project Access Token expires."
  default     = 90
}

variable "rotate_after_days" {
  type        = number
  description = "Number of days before the Project Access Token is automatically rotated."
  default     = 50
}

variable "vault_kv_secret" {
  type = object({
    store        = optional(bool, false)
    mount        = optional(string, "ci")
    name         = optional(string, "")
    auth_path    = optional(string, "")
    project_path = optional(string, "")
    path         = optional(string, "")
    username_key = optional(string, "username")
    token_key    = optional(string, "token")
    extra_data   = optional(map(any), {})
  })
  description = "Optionally store the Project Access Token into Vault. Either `path` or all of `auth_path`, `project_path`, and `name` must be set."
  default     = {}

  validation {
    condition = var.vault_kv_secret.store ? (
      var.vault_kv_secret.path != "" || !contains([var.vault_kv_secret.auth_path, var.vault_kv_secret.project_path, var.vault_kv_secret.name], "")
    ) : true
    error_message = "Either vault_kv_secret.path or all of vault_kv_secret.auth_path, vault_kv_secret.project_path and vault_kv_secret.name must be set when vault_kv_secret.store is enabled."
  }
}

variable "ci_variable" {
  type = object({
    name        = string
    description = optional(string)
    protected   = optional(bool, true)
  })
  description = "Optionally copy the Project Access Token into a CI variable."
  default     = null
  nullable    = true
}
