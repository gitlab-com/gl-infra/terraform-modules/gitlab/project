resource "gitlab_project_access_token" "prat" {
  project = var.project_id

  name         = var.name
  access_level = var.access_level
  scopes       = var.scopes

  rotation_configuration = {
    expiration_days    = var.expires_after_days
    rotate_before_days = var.expires_after_days - var.rotate_after_days
  }
}

resource "gitlab_project_variable" "prat" {
  count = var.ci_variable != null ? 1 : 0

  project     = var.project_id
  key         = var.ci_variable.name
  description = join(" ", [var.ci_variable.description, "(IaC managed)"])
  value       = gitlab_project_access_token.prat.token
  protected   = var.ci_variable.protected
  masked      = true
  raw         = true # not expanded
}
