terraform {
  required_version = ">= 1.4"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 15.0.0"
    }
  }
}
