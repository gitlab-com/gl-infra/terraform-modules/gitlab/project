variable "project_id" {
  type        = number
  description = "ID of the project to create the variables in."
}

variable "vault" {
  type = object({
    addr            = optional(string, "https://vault.ops.gke.gitlab.net")
    auth_path       = string
    ci_shared_path  = optional(string, "shared")
    ci_transit_path = optional(string, "transit/ci")
  })
}
