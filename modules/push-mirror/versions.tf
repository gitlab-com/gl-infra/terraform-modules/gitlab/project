terraform {
  required_version = ">= 1.4"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 15.0.0"

      configuration_aliases = [gitlab.mirror]
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.2"
    }
  }
}
