output "user_id" {
  description = "User ID associated to the Project Access Token for the mirror."
  value       = var.custom_mirror_token != null ? var.custom_mirror_token.user_id : module.prat[0].user_id
}
