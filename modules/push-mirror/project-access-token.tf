module "prat" {
  count  = var.custom_mirror_token == null ? 1 : 0
  source = "../access-token"

  providers = {
    gitlab = gitlab.mirror
  }

  project_id         = data.gitlab_project.mirror.id
  name               = var.token.name
  access_level       = var.token.access_level
  scopes             = var.token.scopes
  expires_after_days = var.token.expires_after_days
  rotate_after_days  = var.token.rotate_after_days
}
