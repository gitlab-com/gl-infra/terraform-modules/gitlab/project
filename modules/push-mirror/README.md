<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 15.0.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | ~> 3.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 15.0.0 |
| <a name="provider_gitlab.mirror"></a> [gitlab.mirror](#provider\_gitlab.mirror) | >= 15.0.0 |
| <a name="provider_null"></a> [null](#provider\_null) | ~> 3.2 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_prat"></a> [prat](#module\_prat) | ../access-token | n/a |

## Resources

| Name | Type |
|------|------|
| [gitlab_branch_protection.branch](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection) | resource |
| [gitlab_project_mirror.mirror](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_mirror) | resource |
| [gitlab_tag_protection.tag](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/tag_protection) | resource |
| [null_resource.mirror-trigger](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [gitlab_project.mirror](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_custom_mirror_token"></a> [custom\_mirror\_token](#input\_custom\_mirror\_token) | Custom Mirror token to use for mirroring, in case the default token is not sufficient | <pre>object({<br/>    username = string,<br/>    user_id  = string,<br/>    token    = string,<br/>  })</pre> | `null` | no |
| <a name="input_ee"></a> [ee](#input\_ee) | Enable GitLab Enterprise features. | `bool` | `true` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Enable the push mirror. | `bool` | `true` | no |
| <a name="input_keep_divergent_refs"></a> [keep\_divergent\_refs](#input\_keep\_divergent\_refs) | Determines if divergent refs are skipped. | `bool` | `false` | no |
| <a name="input_mirror_branch_protections"></a> [mirror\_branch\_protections](#input\_mirror\_branch\_protections) | Protect repository branches.<br/><br/>Example:<br/>{<br/>  my\_branch = {<br/>    merge\_access\_level = "maintainer"<br/>    push\_access\_level  = "no one"<br/>    allowed\_to\_push = {<br/>      user\_ids = [42, 123]<br/>    }<br/>  }<br/>}<br/><br/>Valid access level values: "no one", "developer", or "maintainer" | <pre>map(object({<br/>    code_owner_approval_required = optional(bool)<br/>    merge_access_level           = optional(string, "maintainer")<br/>    push_access_level            = optional(string, "no one")<br/>    unprotect_access_level       = optional(string)<br/>    allow_force_push             = optional(bool)<br/>    allowed_to_merge = optional(object({<br/>      user_ids  = optional(list(number), [])<br/>      group_ids = optional(list(number), [])<br/>    }), {})<br/>    allowed_to_push = optional(object({<br/>      user_ids  = optional(list(number), [])<br/>      group_ids = optional(list(number), [])<br/>    }), {})<br/>    allowed_to_unprotect = optional(object({<br/>      user_ids  = optional(list(number), [])<br/>      group_ids = optional(list(number), [])<br/>    }), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_mirror_project"></a> [mirror\_project](#input\_mirror\_project) | ID or full path of the mirror project. | <pre>object({<br/>    id   = optional(number)<br/>    path = optional(string)<br/>  })</pre> | n/a | yes |
| <a name="input_mirror_tag_protections"></a> [mirror\_tag\_protections](#input\_mirror\_tag\_protections) | Protect repository tags.<br/><br/>Example:<br/>{<br/>  "my\_tag" = {<br/>    create\_access\_level = "developer"<br/>  }<br/>}<br/>Valid values:<br/>"no one":     gitlab.NoPermissions,<br/>"guest":      gitlab.GuestPermissions,<br/>"reporter":   gitlab.ReporterPermissions,<br/>"developer":  gitlab.DeveloperPermissions,<br/>"maintainer": gitlab.MaintainerPermissions,<br/>"owner":      gitlab.OwnerPermission, | <pre>map(object({<br/>    create_access_level = string<br/>    allowed_to_create = optional(object({<br/>      access_level = optional(string)<br/>      user_ids     = optional(list(number), [])<br/>      group_ids    = optional(list(number), [])<br/>    }), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_only_protected_branches"></a> [only\_protected\_branches](#input\_only\_protected\_branches) | Determines if only protected branches are mirrored. | `bool` | `false` | no |
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | ID of the project to configure the push mirror in. | `number` | n/a | yes |
| <a name="input_token"></a> [token](#input\_token) | Settings for the Project Access Token to be created in the mirror project. | <pre>object({<br/>    name               = optional(string, "canonical-repository")<br/>    access_level       = optional(string, "maintainer")<br/>    scopes             = optional(set(string), ["write_repository"])<br/>    expires_after_days = optional(number, 90)<br/>    rotate_after_days  = optional(number, 60)<br/>  })</pre> | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_user_id"></a> [user\_id](#output\_user\_id) | User ID associated to the Project Access Token for the mirror. |
<!-- END_TF_DOCS -->
