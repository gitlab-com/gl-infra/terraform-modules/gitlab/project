resource "null_resource" "mirror-trigger" {
  triggers = {
    uuid = nonsensitive(uuidv5("url", local.mirror_url))
  }
}

resource "gitlab_project_mirror" "mirror" {
  project                 = var.project_id
  enabled                 = var.enabled
  url                     = local.mirror_url
  only_protected_branches = var.only_protected_branches
  keep_divergent_refs     = var.keep_divergent_refs

  lifecycle {
    replace_triggered_by = [
      null_resource.mirror-trigger.id
    ]
  }
}
