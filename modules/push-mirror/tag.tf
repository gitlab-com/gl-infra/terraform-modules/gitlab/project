resource "gitlab_tag_protection" "tag" {
  for_each = var.mirror_tag_protections

  provider = gitlab.mirror

  project = data.gitlab_project.mirror.id

  tag                 = each.key
  create_access_level = each.value.create_access_level

  dynamic "allowed_to_create" {
    for_each = var.ee ? [local.token_user_id] : []
    content { user_id = allowed_to_create.value }
  }

  dynamic "allowed_to_create" {
    for_each = each.value.allowed_to_create.access_level != null ? [1] : []
    content { access_level = each.value.allowed_to_create.access_level }
  }

  dynamic "allowed_to_create" {
    for_each = var.ee ? each.value.allowed_to_create.user_ids : []
    content { user_id = allowed_to_create.value }
  }

  dynamic "allowed_to_create" {
    for_each = var.ee ? each.value.allowed_to_create.group_ids : []
    content { group_id = allowed_to_create.value }
  }
}
