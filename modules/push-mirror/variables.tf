variable "project_id" {
  type        = number
  description = "ID of the project to configure the push mirror in."
}

variable "mirror_project" {
  type = object({
    id   = optional(number)
    path = optional(string)
  })
  description = "ID or full path of the mirror project."

  validation {
    condition     = (var.mirror_project.id != null && var.mirror_project.path == null) || (var.mirror_project.id == null && var.mirror_project.path != null)
    error_message = "Either mirror_project.id or mirror_project.path must be set, but not both."
  }
}

variable "ee" {
  type        = bool
  description = "Enable GitLab Enterprise features."
  default     = true
}

variable "mirror_branch_protections" {
  type = map(object({
    code_owner_approval_required = optional(bool)
    merge_access_level           = optional(string, "maintainer")
    push_access_level            = optional(string, "no one")
    unprotect_access_level       = optional(string)
    allow_force_push             = optional(bool)
    allowed_to_merge = optional(object({
      user_ids  = optional(list(number), [])
      group_ids = optional(list(number), [])
    }), {})
    allowed_to_push = optional(object({
      user_ids  = optional(list(number), [])
      group_ids = optional(list(number), [])
    }), {})
    allowed_to_unprotect = optional(object({
      user_ids  = optional(list(number), [])
      group_ids = optional(list(number), [])
    }), {})
  }))
  description = <<-EOF
    Protect repository branches.

    Example:
    {
      my_branch = {
        merge_access_level = "maintainer"
        push_access_level  = "no one"
        allowed_to_push = {
          user_ids = [42, 123]
        }
      }
    }

    Valid access level values: "no one", "developer", or "maintainer"
    EOF
  default     = {}
}

variable "mirror_tag_protections" {
  type = map(object({
    create_access_level = string
    allowed_to_create = optional(object({
      access_level = optional(string)
      user_ids     = optional(list(number), [])
      group_ids    = optional(list(number), [])
    }), {})
  }))
  description = <<-EOF
    Protect repository tags.

    Example:
    {
      "my_tag" = {
        create_access_level = "developer"
      }
    }
    Valid values:
    "no one":     gitlab.NoPermissions,
    "guest":      gitlab.GuestPermissions,
    "reporter":   gitlab.ReporterPermissions,
    "developer":  gitlab.DeveloperPermissions,
    "maintainer": gitlab.MaintainerPermissions,
    "owner":      gitlab.OwnerPermission,
    EOF
  default     = {}
}

variable "token" {
  type = object({
    name               = optional(string, "canonical-repository")
    access_level       = optional(string, "maintainer")
    scopes             = optional(set(string), ["write_repository"])
    expires_after_days = optional(number, 90)
    rotate_after_days  = optional(number, 60)
  })
  description = "Settings for the Project Access Token to be created in the mirror project."
  default     = {}
}

variable "enabled" {
  type        = bool
  description = "Enable the push mirror."
  default     = true
}

variable "only_protected_branches" {
  type        = bool
  description = "Determines if only protected branches are mirrored."
  default     = false
}

variable "keep_divergent_refs" {
  type        = bool
  description = "Determines if divergent refs are skipped."
  default     = false
}

variable "custom_mirror_token" {
  type = object({
    username = string,
    user_id  = string,
    token    = string,
  })
  sensitive   = true
  description = "Custom Mirror token to use for mirroring, in case the default token is not sufficient"
  default     = null
}
