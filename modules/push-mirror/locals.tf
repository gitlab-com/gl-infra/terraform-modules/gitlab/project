locals {
  username      = nonsensitive(var.custom_mirror_token) != null ? nonsensitive(var.custom_mirror_token.username) : var.token.name
  token         = nonsensitive(var.custom_mirror_token) != null ? var.custom_mirror_token.token : module.prat[0].token
  token_user_id = nonsensitive(var.custom_mirror_token) != null ? nonsensitive(var.custom_mirror_token.user_id) : module.prat[0].user_id

  # checkov:skip=CKV_SECRET_4:This URL format string doesn't contain any actual credentials
  mirror_url = format("https://%s:%s@%s",
    local.username,
    local.token,
    trimprefix(data.gitlab_project.mirror.http_url_to_repo, "https://")
  )
}
