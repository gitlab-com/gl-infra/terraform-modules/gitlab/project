data "gitlab_project" "mirror" {
  provider = gitlab.mirror

  id                  = var.mirror_project.id
  path_with_namespace = var.mirror_project.path
}
