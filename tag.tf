resource "gitlab_tag_protection" "tag" {
  // Remove the default "*" tag if `republishes_truncated_version_tags` is set.
  for_each = var.archived ? {} : { for tag, v in var.tag_protections : tag => v if tag != "*" || !local.common_ci_tasks.republishes_truncated_version_tags }

  project = local.project_id

  tag                 = each.key
  create_access_level = each.value.create_access_level

  dynamic "allowed_to_create" {
    for_each = each.value.allowed_to_create.access_level != null ? [1] : []
    content { access_level = each.value.allowed_to_create.access_level }
  }

  dynamic "allowed_to_create" {
    for_each = var.ee ? each.value.allowed_to_create.user_ids : []
    content { user_id = allowed_to_create.value }
  }

  dynamic "allowed_to_create" {
    for_each = var.ee ? each.value.allowed_to_create.group_ids : []
    content { group_id = allowed_to_create.value }
  }
}
