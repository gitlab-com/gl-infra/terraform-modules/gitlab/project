# Resources supporting CI tasks from https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks

# https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/goreleaser.md

module "prat_common_ci_tasks_goreleaser" {
  count = local.common_ci_tasks.goreleaser ? 1 : 0

  source = "./modules/access-token"

  project_id   = local.project_id
  name         = "goreleaser"
  access_level = "maintainer"
  scopes       = ["api"]

  vault_kv_secret = {
    store        = local.vault_enabled
    mount        = local.vault_ci_path
    auth_path    = var.vault.auth_path
    project_path = local.project_path_with_namespace
    name         = "goreleaser"
  }
}

# https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/mirroring.md

module "prat_common_ci_tasks_mirroring" {
  count = local.common_ci_tasks.mirroring ? 1 : 0

  source = "./modules/access-token"

  project_id   = local.project_id
  name         = "mirror"
  access_level = "reporter"
  scopes       = ["api"]

  vault_kv_secret = {
    store        = local.vault_enabled
    mount        = local.vault_ci_path
    auth_path    = var.vault.auth_path
    project_path = local.project_path_with_namespace
    name         = "mirroring"
  }
}

# https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.md

module "prat_common_ci_tasks_renovate_bot" {
  count = local.common_ci_tasks.renovate_bot ? 1 : 0

  source = "./modules/access-token"

  project_id   = local.project_id
  name         = "renovate-bot"
  access_level = "maintainer"
  scopes       = ["api", "read_registry", "write_repository"]

  vault_kv_secret = {
    store        = local.vault_enabled
    mount        = local.vault_ci_path
    auth_path    = var.vault.auth_path
    project_path = local.project_path_with_namespace
    name         = "renovate-bot"
  }
}

resource "random_integer" "common_ci_tasks_renovate_bot_minute" {
  count = local.common_ci_tasks.renovate_bot ? 1 : 0
  min   = 0
  max   = 59
}

resource "random_integer" "common_ci_tasks_renovate_bot_hour" {
  count = local.common_ci_tasks.renovate_bot ? 1 : 0
  min   = 0
  max   = 23
}

# Scheduled Renovate job

resource "gitlab_pipeline_schedule" "common_ci_tasks_renovate_bot" {
  count = local.common_ci_tasks.renovate_bot ? 1 : 0

  project       = local.project_id
  description   = "Renovate"
  ref           = "refs/heads/${coalesce(var.default_branch, "main")}"
  cron          = coalesce(var.common_ci_tasks.renovate_bot_schedule, format("%d %d * * *", random_integer.common_ci_tasks_renovate_bot_minute[0].result, random_integer.common_ci_tasks_renovate_bot_hour[0].result))
  cron_timezone = "Etc/UTC"
  active        = true
}

resource "gitlab_pipeline_schedule_variable" "common_ci_tasks_renovate_bot_scheduled" {
  count = local.common_ci_tasks.renovate_bot ? 1 : 0

  project              = local.project_id
  pipeline_schedule_id = gitlab_pipeline_schedule.common_ci_tasks_renovate_bot[0].pipeline_schedule_id
  key                  = "RENOVATE_SCHEDULED"
  value                = "1"
}

# Manually triggered Renovate job

resource "gitlab_pipeline_schedule" "common_ci_tasks_renovate_bot_immediate" {
  count = local.common_ci_tasks.renovate_bot ? 1 : 0

  project       = local.project_id
  description   = "Renovate now!"
  ref           = "refs/heads/${coalesce(var.default_branch, "main")}"
  cron          = "0 1 * * *"
  cron_timezone = "Etc/UTC"
  active        = false
}

resource "gitlab_pipeline_schedule_variable" "common_ci_tasks_renovate_bot_immediate" {
  count = local.common_ci_tasks.renovate_bot ? 1 : 0

  project              = local.project_id
  pipeline_schedule_id = gitlab_pipeline_schedule.common_ci_tasks_renovate_bot_immediate[0].pipeline_schedule_id
  key                  = "RENOVATE_IMMEDIATE"
  value                = "1"
}

resource "gitlab_tag_protection" "common_ci_tasks_renovate_bot_protected_tags" {
  count = local.common_ci_tasks.republishes_truncated_version_tags ? 1 : 0

  project             = local.project_id
  tag                 = "v*.*.*"
  create_access_level = "maintainer"
}

# https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/semantic-release.md

module "prat_common_ci_tasks_semantic_release" {
  count = local.common_ci_tasks.semantic_release ? 1 : 0

  source = "./modules/access-token"

  project_id   = local.project_id
  name         = "semantic-release"
  access_level = "maintainer"
  scopes       = ["api", "write_repository"]

  vault_kv_secret = {
    store        = local.vault_enabled
    mount        = local.vault_ci_path
    auth_path    = var.vault.auth_path
    project_path = local.project_path_with_namespace
    name         = "semantic-release"
  }
}

# https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/danger.md

module "prat_common_ci_tasks_danger" {
  count = local.common_ci_tasks.danger ? 1 : 0

  source = "./modules/access-token"

  project_id   = local.project_id
  name         = "danger"
  access_level = "developer"
  scopes       = ["api"]

  vault_kv_secret = {
    store        = local.vault_enabled
    mount        = local.vault_ci_path
    auth_path    = var.vault.auth_path
    project_path = local.project_path_with_namespace
    name         = "danger"
  }
}

# https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/autolabels.md

module "prat_common_ci_tasks_autolabels" {
  count = local.common_ci_tasks.autolabels ? 1 : 0

  source = "./modules/access-token"

  project_id   = local.project_id
  name         = "Autolabels Bot"
  access_level = "developer"
  scopes       = ["api", "read_repository"]

  vault_kv_secret = {
    store        = local.vault_enabled
    mount        = local.vault_ci_path
    auth_path    = var.vault.auth_path
    project_path = local.project_path_with_namespace
    name         = "autolabels"
  }
}

resource "gitlab_project_variable" "common_ci_tasks_autolabels_enabled" {
  count = var.common_ci_tasks.enabled ? 1 : 0

  project           = local.project_id
  key               = "AUTOLABELS_ENABLED"
  value             = local.common_ci_tasks.autolabels ? "true" : "false"
  description       = "Controls whether the \"autolabels\" job from common-ci-tasks runs. (IaC managed)"
  raw               = true
  protected         = false
  masked            = false
  environment_scope = "*"
}
